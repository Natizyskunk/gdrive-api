# Gdrive-API-Linux-x64
Gdrive-API is a command line utility for interacting with Google Drive.

## How-To.
Please enter those commands:
```
apt-get install git
apt-get update
apt-get dist-upgrade
git clone https://github.com/Natizyskunk/gdrive-api.git
cd gdrive-api/
chmod +x install-gdrive-api.sh
bash install-gdrive-api.sh
```
Then follow the instructions.

## Commands.
**List all documents on Gdrive:** <BR />
$ gdrive list

**Get file info:** <BR />
$ gdrive info [dir ID] <BR />
example: $ gdrive info 0B3X9GlR6EmbnNTk0SkV0bm5Hd0E

**Make directory:** <BR />
$ gdrive mkdir MyDirectory <BR />
example: $ gdrive mkdir pictures

**Upload:** <BR />
$ gdrive upload --parent [dir ID] [filename] <BR />
example: $ gdrive upload --parent 1BM49vIOIZBm1EBSqMCG9clJjE7hjUD7G C:/pictures/cat.png

**Download:** <BR />
$ gdrive download [dir ID] <BR />
example: $ gdrive download 0F8T9GdQ0ENbmZ5ACU81ZdEVgYRd


## Sources.
- Linux GoogleDrive API: https://github.com/prasmussen/gdrive