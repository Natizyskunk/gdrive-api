@echo off

echo "******************************************************************"
echo "*                                                                *"
echo "*  Gdrive-API-Windos-x64 Install script by Natizyskunk@Github.   *"
echo "* This script will install and configure your Gdrive-API Access. *"
echo "*                                                                *"
echo "******************************************************************"
echo.

:choice
set /P c=Are you sure you want to continue[Y/N]?
if /I "%c%" EQU "Y" goto :somewhere
if /I "%c%" EQU "N" goto :somewhere_else
goto :choice

:somewhere
echo --------
echo !!!! README !!!!
echo.
echo 1. You will be asked to enter a verification code, which is obtained by heading to the url printed in the command's output and authenticating with the google account for the drive you want access to. The following screenshots will make the process clear !
echo -
echo 2. After the verification code is entered on the command line terminal, Gdrive will connect to your Google Drive account !
echo.
echo.
echo Copying gdrive-api to '%UserProfile%' folder
mkdir "%UserProfile%\gdrive-api"
copy "Apps\gdrive-2.1.0+Go-1.7.4-windows-x64.exe" "%UserProfile%\gdrive-api\gdrive.exe"

echo Adding Gdrive-API to the system environment variables.
setx path "%path%;C:\gdrive-api\"
echo.
echo.

echo ----------------
echo !!!! Instructions !!!!
echo Creating and asking google Link Authentification permissions.
echo Please read & follow instructions.
gdrive about
echo.
echo.

echo ----------------
echo !!!! Important note !!!!
echo.
echo - The aforementioned authentication process will create a token file inside a folder named .gdrive located in your %AppData% directory.
echo - Note that anyone with access to this file will also have access to your Google drive !!
echo.
echo.
@pause
exit

:somewhere_else
echo Installation has been aborted !
@pause
exit