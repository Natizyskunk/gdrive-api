#!/bin/bash

echo "****************************************************************************"
echo "*                                                                          *"
echo "*       Gdrive-API-linux64 VPS Install script by Natizyskunk@Github.       *"
echo "*      This script will install and configure your Gdrive-API Access.   	 *"
echo "*                                                                          *"
echo "****************************************************************************"
echo && echo && echo
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo "!  																					                                               !"
echo "!     You will be asked to enter a verification code, which is obtained by heading to the url printed in the command's output !	   !"
echo "! and authenticating with the google account for the drive you want access to. The following screenshots will make the process clear !"
echo "!       After the verification code is entered on the command line terminal, Gdrive will connect to your Google Drive account.       !"
echo "!  																					                                               !"
echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
echo && echo && echo 

echo "Do you really want to install Gdrive-API (no if you did it before)? [y/n]"
read DOSETUP

if [[ $DOSETUP =~ "y" ]] ; 
  then
	function install_gdrive {
		chmod +x ./Apps/gdrive*
		cp Apps/gdrive-2.1.0+Go-1.7.4-linux-x64 /usr/bin/gdrive
		gdrive about
	}
	
	function important_note {
		echo && echo && echo
		echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
		echo "!  																					                                                  !"
		echo "!													    !!!!! Important note !!!!!													      !"
		echo "! The aforementioned authentication process will create a token file inside a folder named .gdrive located in your %AppData% directory. !"
		echo "!       				  Note that anyone with access to this file will also have access to your Google drive !!       				  !"
		echo "!  																					                                                  !"
		echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
		echo && echo && echo
	}

	##### Main #####
	clear
	install_gdrive
	important_note
else
	echo "Installation has been aborted !"
fi